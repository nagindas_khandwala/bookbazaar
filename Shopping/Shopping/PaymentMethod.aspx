﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethod.aspx.cs" Inherits="Shopping.PaymentMethod" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="PM" runat="server">
<div align="center">
<h4><b>Payment Method</b></h4>
<hr />
</div>
<table align="center" cellspacing="1" style="width: 60%; background-color: #FFFFFF;">
<tr>
<td style="width: 50%; padding-left: 100px;" align="left">
<b>Card Number :</b>
</td>
<td style="width: 50%;" align="left">
    <asp:TextBox ID="txtCardNumber" runat="server" Width="212px"></asp:TextBox>
   
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"  ControlToValidate="txtCardNumber" ForeColor="Red" ></asp:RequiredFieldValidator>

</td>
</tr>

<tr>
<td style="width: 50%;" align="right">&nbsp;</td>
<td style="width: 50%;" align="left">&nbsp;</td>
</tr>

<tr>
<td style="width: 50%; padding-left: 100px;" align="left">
<b>Expiry Date :</b>
</td>
<td style="width: 50%;" align="left">
   <asp:TextBox ID="txtExpiryDate" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtExpiryDate" ForeColor="Red"></asp:RequiredFieldValidator>
</td>
</tr>


<tr>
<td style="width: 50%;" align="right">&nbsp;</td>
<td style="width: 50%;" align="left">&nbsp;</td>
</tr>


<tr>
<td style="width: 50%; padding-left: 100px;" align="left">
<b>CVV :</b>
</td>
<td style="width: 50%;" align="left">
    <asp:TextBox ID="txtCVV" runat="server" Width="212px" MaxLength="16"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtCVV" ForeColor="Red"></asp:RequiredFieldValidator>
</td>
</tr>

<tr>
<td style="width: 50%;" align="right">&nbsp;</td>
<td style="width: 50%;" align="left">&nbsp;</td>
</tr>


<tr>
<td style="width: 50%; padding-left: 100px;" align="left">
&nbsp;
</td>
<td style="width: 50%;" align="left">
    <asp:Button ID="btnData" runat="server" Text="SubmitData" Width="100px" 
        Height="30px" onclick="btnData_Click"/>
    &nbsp;</td>
</tr>

<tr>
<td style="width: 50%;" align="right">&nbsp;</td>
<td style="width: 50%;" align="left">&nbsp;</td>
</tr>


</table>

</form>
</body>
</html>
