﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AddNewProducts.aspx.cs" Inherits="Shopping.Admin.AddNewProducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center">
<h4>
<b>Add New Products</b>
</h4>
<hr />
</div>
<table align="center" cellspacing="1" style="width: 70%; background-color: #FFFFFF;">


<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Product Name :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtProductName" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RProductName" ControlToValidate="txtProductName" runat="server" 
    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator></td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Product Category :</b></td>
<td style="width: 50%;" align="left">
    <asp:DropDownList ID="dd1Category" runat="server" Width="212px">
    </asp:DropDownList>
</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Product Description :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtProductDescription" runat="server" Width="212px" Height="80px" TextMode="MultiLine"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RDescription" ControlToValidate="txtProductDescription" runat="server" 
    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator></td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Product Image :</b></td>
<td style="width:50%;" align ="left">
    <asp:FileUpload ID="uploadProductPhoto" runat="server" />
    <asp:RequiredFieldValidator ID="RProductPhoto" ControlToValidate="uploadProductPhoto" runat="server" 
    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator></td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Product Price :</b></td>
    <td style="width:50%;" align ="left">
    <asp:TextBox ID="txtProductPrice" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RProductPrice" ControlToValidate="txtProductPrice" runat="server" 
    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="REProductPrice" runat="server" ControlToValidate="txtProductPrice"
    ErrorMessage="Number" ForeColor="Red"></asp:RegularExpressionValidator>
    </td>
    </tr>

<tr>
<td style="width:50%;" align ="right">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Product Quantity :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtProductQuantity" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RProductQuantity" ControlToValidate="txtProductQuantity" runat="server" 
    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="REProductQuantity" runat="server" ControlToValidate="txtProductQuantity"
    ErrorMessage="Number" ForeColor="Red"></asp:RegularExpressionValidator></td>
</tr>

<tr>
<td style="width:50%;" align ="right">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>
<tr>
<td style="width:50%;" align ="right">&nbsp;</td>
<td style="width:50%;" align ="left"> 
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" Height="30px" OnClick="btnSubmit_Click" />
</td>
</tr>
<tr>
<td align="center" colspan="2">
    <asp:Label ID="Label1" runat="server" ForeColor="Blue"></asp:Label>
</td>
</tr>
</table>
</asp:Content>
