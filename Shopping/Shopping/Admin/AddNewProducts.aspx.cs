﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Shopping.BusinessLayer;

namespace Shopping.Admin
{
    public partial class AddNewProducts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCategories();

                AddSubmitEvent();

                if (Request.QueryString["alert"] == "success")
                {
                    Response.Write("<script>('Record Saved Successfully.');</script>");
                }
            }
        }

        private void AddSubmitEvent()
        {
            UpdatePanel updatePanel = Page.Master.FindControl("AdminUpdatePanel") as UpdatePanel;
            UpdatePanelControlTrigger trigger = new PostBackTrigger();
            trigger.ControlID = btnSubmit.UniqueID;
            updatePanel.Triggers.Add(trigger);
        }

        private void GetCategories()
        {
            ShoppingCart k = new ShoppingCart();
            DataTable dt = k.GetCategories();
            if (dt.Rows.Count > 0)
            {
                dd1Category.DataValueField = "CategoryID";
                dd1Category.DataTextField = "CategoryName";
                dd1Category.DataSource = dt;
                dd1Category.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (uploadProductPhoto.PostedFile != null)
            {
                SaveProductPhoto();

                ShoppingCart k = new ShoppingCart()
                {
                    ProductName = txtProductName.Text,
                    ProductImage = "~/ProductImages/" + uploadProductPhoto.FileName,
                    ProductPrice = txtProductPrice.Text,
                    ProductDescription = txtProductDescription.Text,
                    CategoryID = Convert.ToInt32(dd1Category.SelectedValue),
                    TotalProducts = Convert.ToInt32(txtProductQuantity.Text)
                };

                k.AddNewProduct();               
                ClearText();
                Label1.Text = "Inserted Successfully..";
                Response.Redirect("~/Admin/AddNewProducts.aspx?alert=success");
                

            }

            else
            {
                Response.Write("<script>alert('Please Upload Photo');</script>");
            }

        }

        private void ClearText()
        {
            uploadProductPhoto = null;
            txtProductName.Text = string.Empty;
            txtProductPrice.Text = string.Empty;
            txtProductDescription.Text = string.Empty;
            txtProductQuantity.Text = string.Empty;
        }

        private void SaveProductPhoto()
        {
            if (uploadProductPhoto.PostedFile != null)
            {
                string filename = uploadProductPhoto.PostedFile.FileName.ToString();
                string fileExt = System.IO.Path.GetExtension(uploadProductPhoto.FileName);


                if (filename.Length > 96)
                {
                    Response.Write("Filename should not be greater than 96");
                }

                else if (fileExt != ".jpeg" && fileExt != ".jpg" && fileExt != ".bmp")
                {

                }

                else if (uploadProductPhoto.PostedFile.ContentLength > 4000000)
                {

                }

                else
                {
                    uploadProductPhoto.SaveAs(Server.MapPath("~/ProductImages/" + filename));
                    
                }


            }
        

        }
    }
}