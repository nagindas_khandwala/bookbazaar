﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Seller/SellerMaster.Master" AutoEventWireup="true" CodeBehind="AddBooks.aspx.cs" Inherits="Shopping.Seller.AddBooks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div align="center">
<h4>
<b>Add New Books</b>
</h4>
<hr />
</div>
<table align="center" cellspacing="1" style="width: 60%; background-color: #FFFFFF;">

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Seller Name :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtName" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
    ErrorMessage="*" ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="[a-zA-Z]*$" 
    ErrorMessage="Must be Characters" ControlToValidate="txtName" ForeColor="Red"></asp:RegularExpressionValidator>
    </td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Seller Contact No. :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtContact" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
    ErrorMessage="*" ControlToValidate="txtContact" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\d+"
    ErrorMessage="Must Be a Number" ControlToValidate="txtContact" ForeColor="Red"></asp:RegularExpressionValidator>
    </td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>



<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Book Name :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtBookName" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
    ErrorMessage="*" ControlToValidate="txtBookName" ForeColor="Red"></asp:RequiredFieldValidator>
    </td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Book Category :</b></td>
<td style="width: 50%;" align="left">
    <asp:DropDownList ID="dd2Category" runat="server" Width="212px">
    </asp:DropDownList>
</td>
</tr>


<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Book Description :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtBookDescription" runat="server" Width="212px" Height="80px" TextMode="MultiLine"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
    ErrorMessage="*" ControlToValidate="txtBookDescription" ForeColor="Red"></asp:RequiredFieldValidator>
    </td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Book Image :</b></td>
<td style="width:50%;" align ="left">
    <asp:FileUpload ID="uploadBookPhoto" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
    ErrorMessage="*" ControlToValidate="uploadBookPhoto" ForeColor="Red"></asp:RequiredFieldValidator>
</td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>
<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Book Price :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtBookPrice" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
    ErrorMessage="*" ControlToValidate="txtBookPrice" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"  ValidationExpression="\d+" 
    ErrorMessage="Must be A Number" ControlToValidate="txtBookPrice" ForeColor="Red"></asp:RegularExpressionValidator>
    </td>
</tr>

<tr>
<td style="width:50%;" align ="right">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>

<tr>
<td style="width:50%; padding-left: 100px;" align ="left"><b>Book Quantity :</b></td>
<td style="width:50%;" align ="left">
    <asp:TextBox ID="txtBookQuantity" runat="server" Width="212px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
    ErrorMessage="*" ControlToValidate="txtBookQuantity" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"  ValidationExpression="\d+" 
    ErrorMessage="Must be A Number" ControlToValidate="txtBookQuantity" ForeColor="Red"></asp:RegularExpressionValidator>
    </td>
</tr>

<tr>
<td style="width:50%;" align ="right">&nbsp;</td>
<td style="width:50%;" align ="left"> &nbsp;</td>
</tr>
<tr>
<td style="width:50%;" align ="right">&nbsp;</td>
<td style="width:50%;" align ="left"> 
    <asp:Button ID="btnAddBooks" runat="server" Text="AddBooks" Width="100px" 
        Height="30px" OnClick="btnAddBooks_Click"/>
</td>
</tr>
<tr>
<td align="center" colspan="2">
    <asp:Label ID="Label1" runat="server"></asp:Label>
</td>
</tr>
</table>

   




</asp:Content>
