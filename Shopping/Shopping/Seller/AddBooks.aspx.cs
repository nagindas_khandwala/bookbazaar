﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Shopping.BusinessLayer;

namespace Shopping.Seller
{
    public partial class AddBooks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCategories();
                AddSubmitEvent();
            }
        }

        private void AddSubmitEvent()
        {
            UpdatePanel updatePanel1 = Page.Master.FindControl("SellerUpdatePanel") as UpdatePanel;
            UpdatePanelControlTrigger trigger = new PostBackTrigger();
            trigger.ControlID = btnAddBooks.UniqueID;
            updatePanel1.Triggers.Add(trigger);
        }

        private void GetCategories()
        {
            ShoppingCart k = new ShoppingCart();
            DataTable dt = k.GetCategories();
            if (dt.Rows.Count > 0)
            {
                dd2Category.DataValueField = "CategoryID";
                dd2Category.DataTextField = "CategoryName";
                dd2Category.DataSource = dt;
                dd2Category.DataBind();
            }
        }

        protected void btnAddBooks_Click(object sender, EventArgs e)
        {
            if (uploadBookPhoto.PostedFile != null)
            {
                SaveBookPhoto();

                ShoppingCart k = new ShoppingCart()
                {
                    SellerName = txtName.Text,
                    SellerContact = txtContact.Text,
                    BookName = txtBookName.Text,
                    BookImage = "~/BooksImages/" + uploadBookPhoto.FileName,
                    BookPrice = txtBookPrice.Text,
                    BookDescription = txtBookDescription.Text,
                    CategoryID = Convert.ToInt32(dd2Category.SelectedValue),
                    TotalBooks = Convert.ToInt32(txtBookQuantity.Text)
                };

                k.AddNewBook();
                ClearText();
                Label1.Text = "Inserted Successfully..";



            }

            else
            {
                Response.Write("<script>alert('Please Upload Photo');</script>");
            }

        }

        private void ClearText()
        {
            txtName.Text = null;
            txtContact.Text = null;
            uploadBookPhoto = null;
            txtBookName.Text = string.Empty;
            txtBookPrice.Text = string.Empty;
            txtBookDescription.Text = string.Empty;
            txtBookQuantity.Text = string.Empty;
        }

        private void SaveBookPhoto()
        {
            if (uploadBookPhoto.PostedFile != null)
            {
                string filename = uploadBookPhoto.PostedFile.FileName.ToString();
                string fileExt = System.IO.Path.GetExtension(uploadBookPhoto.FileName);


                if (filename.Length > 96)
                {
                    Response.Write("Filename should not be greater than 96");
                }

                else if (fileExt != ".jpeg" && fileExt != ".jpg" && fileExt != ".bmp")
                {

                }

                else if (uploadBookPhoto.PostedFile.ContentLength > 4000000)
                {

                }

                else
                {
                    uploadBookPhoto.SaveAs(Server.MapPath("~/BooksImages/" + filename));

                }


            }


        }



    }

}